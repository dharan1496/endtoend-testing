exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['first.js'],
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
     args: [ '--headless', '--no-sandbox', '--disable-gpu', '--        window-size=1920,1080' ]
    }
    }
};